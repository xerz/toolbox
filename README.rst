@xerz's Tiny Terminal Toolbox
================================================================================

Welcome! This repo contains some useful small commands which I've coded for my
(GNU)Linux installations in response to my urgent need for awesome ways to run
my awesome tools.

- ``edit`` is a quick and simple way to launch an Emacs session
- ``inferno`` comfortably launches an Inferno emulator
- ``media`` improves ``youtube-dl`` by enabling metadata options by default
- ``passgen`` generates a list of strings which can be used as passwords
- ``run`` launches and detaches from your shell whatever you tell it to
- ``shot`` takes care of the boring details of taking screenshots with ``maim``
- ``startvt`` opens a graphical KMS virtual terminal with ``kmscon``
- ``surft`` makes Suckless' ``surf`` web browser friendlier by adding tabs

Requirements
--------------------------------------------------------------------------------

For all commands:

- A decent \*nix kernel

	- Linux (https://github.com/torvalds/linux)
	- FreeBSD (https://www.freebsd.org)
	- OpenBSD (https://www.openbsd.org)
	- NetBSD (https://www.netbsd.org)
	- XNU (https://opensource.apple.com/source/xnu)

- An IEEE POSIX.1-2017 compliant Unix shell

	- Almquist Shell (https://www.in-ulm.de/~mascheck/various/ash)
	- GNU Bash (https://www.gnu.org/software/bash)
	- Zsh (https://github.com/zsh-users/zsh)
	- BusyBox (https://www.busybox.net)
	- Policy-compliant Ordinary Shell (https://packages.debian.org/stable/shells/posh)
	- KornShell (https://github.com/att/ast)

``edit``:

- An Emacs implementation with ``emacsclient``

	- GNU Emacs (https://www.gnu.org/software/emacs)
	- XEmacs (https://www.xemacs.org)

``inferno``:

- Inferno (http://www.vitanuova.com/inferno)

``media``:

- ``youtube-dl`` (https://rg3.github.io/youtube-dl)
- ``ffmpeg`` (https://ffmpeg.org)
- AtomicParsley (https://sourceforge.net/projects/atomicparsley)
- ``attr`` (https://savannah.nongnu.org/projects/attr)

``passgen``:

- IEEE POSIX.1-2017 compliant implementations of ``fold`` and ``tr``

	- GNU Coreutils (https://www.gnu.org/software/coreutils)
	- BusyBox (https://www.busybox.net)

``run``:

- An IEEE POSIX.1-2017 compliant ``nohup`` implementation

        - GNU Coreutils (https://www.gnu.org/software/coreutils)
        - BusyBox (https://www.busybox.net)

``shot``:

- ``maim`` (https://github.com/naelstrof/maim)

``startvt``:

- An IEEE POSIX.1-2017 compliant ``env`` implementation

	- GNU Coreutils (https://www.gnu.org/software/coreutils)
	- BusyBox (https://www.busybox.net)

- ``kmscon`` (https://github.com/dvdhrm/kmscon)

``surft``:

- ``tabbed`` (https://tools.suckless.org/tabbed)
- ``surf`` (https://surf.suckless.org)

License
--------------------------------------------------------------------------------

All of the script files shared under this repository, as well as this document,
are licensed as `CC0 <https://creativecommons.org/publicdomain/zero/1.0/>`_.
I mean, it's not like it's worth GPLing, right...?
